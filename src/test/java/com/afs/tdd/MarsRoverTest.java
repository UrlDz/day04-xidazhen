package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTest {
    //move
    @Test
    void should_change_to_0_1_North_when_executeCommand_given_location_0_0_North_and_command_move() {
        Location locationInitial = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_1_South_when_executeCommand_given_location_0_2_South_and_command_move() {
        Location locationInitial = new Location(0, 2, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_Easter_when_executeCommand_given_location_0_0_Easter_and_command_move() {
        Location locationInitial = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.EAST, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_West_when_executeCommand_given_location_2_0_West_and_command_move() {
        Location locationInitial = new Location(2, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.MOVE;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.WEST, locationAfterMove.getDirection());
    }

    //turnLeft
    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_North_and_command_turnLeft() {
        Location locationInitial = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TURN_LEFT;
        Location locationTurnLeft= marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.WEST, locationTurnLeft.getDirection());
    }

    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_West_and_command_turnLeft() {
        Location locationInitial = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TURN_LEFT;
        Location locationTurnLeft = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH, locationTurnLeft.getDirection());
    }

    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_South_and_command_turnLeft() {
        Location locationInitial = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TURN_LEFT;
        Location locationTurnLeft = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.EAST, locationTurnLeft.getDirection());
    }

    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_East_and_command_turnLeft() {
        Location locationInitial = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TURN_LEFT;
        Location locationTurnLeft = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationTurnLeft.getCoordinateX());
        Assertions.assertEquals(0, locationTurnLeft.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH, locationTurnLeft.getDirection());
    }

    //turnRight
    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_North_and_command_turnRight() {
        Location locationInitial = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TURN_RIGHT;
        Location locationTurnRight = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationTurnRight.getCoordinateX());
        Assertions.assertEquals(0, locationTurnRight.getCoordinateY());
        Assertions.assertEquals(Direction.EAST, locationTurnRight.getDirection());
    }

    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_East_and_command_turnRight() {
        Location locationInitial = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TURN_RIGHT;
        Location locationTurnRight = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationTurnRight.getCoordinateX());
        Assertions.assertEquals(0, locationTurnRight.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH, locationTurnRight.getDirection());
    }

    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_South_and_command_turnRight() {
        Location locationInitial = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TURN_RIGHT;
        Location locationTurnRight = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationTurnRight.getCoordinateX());
        Assertions.assertEquals(0, locationTurnRight.getCoordinateY());
        Assertions.assertEquals(Direction.WEST, locationTurnRight.getDirection());
    }

    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_West_and_command_turnRight() {
        Location locationInitial = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TURN_RIGHT;
        Location locationTurnRight = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0, locationTurnRight.getCoordinateX());
        Assertions.assertEquals(0, locationTurnRight.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH, locationTurnRight.getDirection());
    }

    @Test
    void should_change_to_1_2_North_when_executeCommand_given_location_0_0_North_and_multiple_commands() {
//        MRMLM -> 0,1  1,1  1,2
        Location locationInitial = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(locationInitial);
        Location locationAfterMultipleMove = marsRover.executeMultipleCommands("MRMLM");
        Assertions.assertEquals(1, locationAfterMultipleMove.getCoordinateX());
        Assertions.assertEquals(2, locationAfterMultipleMove.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH, locationAfterMultipleMove.getDirection());
    }

}
