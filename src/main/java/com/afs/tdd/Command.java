package com.afs.tdd;

public enum Command {
    MOVE('M'), TURN_LEFT('L'), TURN_RIGHT('R');
    private char abbreviation;

    Command(char abbreviation) {
        this.abbreviation = abbreviation;
    }

    public static Command transferToChar(char inputAbbreviation) {
        for (Command command : values()) {
            if (command.abbreviation == inputAbbreviation) {
                return command;
            }
        }
        throw new IllegalArgumentException("No constant for character " + inputAbbreviation + " found");
    }
}
