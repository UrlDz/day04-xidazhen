package com.afs.tdd;

import java.util.stream.IntStream;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeMultipleCommands(String commands){
        IntStream commandStream = commands.chars();
        commandStream.mapToObj(command -> (char)command).forEach((command) -> executeCommand(Command.transferToChar(command)));
        return location;
    }
    public Location executeCommand(Command command) {
        switch (command) {
            case MOVE:
                move();
                break;
            case TURN_LEFT:
                turnLeft();
                break;
            case TURN_RIGHT:{
                turnRight();
            }
        }

        return location;
    }
    public void move(){
        switch (location.getDirection()) {
            case NORTH:
                location.setCoordinateY(location.getCoordinateY() + 1);
                break;
            case SOUTH:
                location.setCoordinateY(location.getCoordinateY() - 1);
                break;
            case EAST:
                location.setCoordinateX(location.getCoordinateX() + 1);
                break;
            case WEST:
                location.setCoordinateX(location.getCoordinateX() - 1);
        }
    }

    public void turnLeft(){
        switch (location.getDirection()){
            case NORTH:
                location.setDirection(Direction.WEST);
                break;
            case WEST:
                location.setDirection(Direction.SOUTH);
                break;
            case SOUTH:
                location.setDirection(Direction.EAST);
                break;
            case EAST:
                location.setDirection(Direction.NORTH);
        }
    }

    public void turnRight(){
        switch (location.getDirection()){
            case NORTH:
                location.setDirection(Direction.EAST);
                break;
            case EAST:
                location.setDirection(Direction.SOUTH);
                break;
            case SOUTH:
                location.setDirection(Direction.WEST);
                break;
            case WEST:
                location.setDirection(Direction.NORTH);
        }
    }
}
